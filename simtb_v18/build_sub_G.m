function [subG] = build_sub_G(sP, sub, delay, timebins)
%% Builds G Matrix for only one subject

filename = simtb_makefilename(sP, 'SIM', sub);

SIM = load(filename);

% SIM. blocks is a #scanspersubject X #blocks matrix of zeros and ones.
% A non-zero entry in row t and column b means that the experiment is in
% block b for scan t
blocks = SIM.blocks;

for col = 1:size(SIM.blocks,2) % go through all blocks
    row = 2;
    
    while row <= size(SIM.blocks,1)  % while current row index does not exceed #scans per subject
        
        % If this scan is the initial scan for a block, ...
        if SIM.blocks(row,col) == 1 && SIM.blocks(row-1,col) == 0
            % ... the non-zero entry remains.
            blocks(row, col) = 1;
        
        % If the current scan is not the first scan of a new block, ...
        else 
            % set this entry to zero
            blocks(row, col) = 0;
        end
        
        row = row+1;
    end
    
end

% now, blocks should be a matrix indicating the initial scans for all
% blocks

% initialize G matrix for one subject, it # scans per subject + # timebins
% many rows and # timebins * # blocks many columns
subG = zeros(size(SIM.blocks,1)+timebins, timebins*(size(SIM.blocks,2)));

for col = 1:size(SIM.blocks,2)
    
    stimulus_start_rows = find(blocks(:,col)); % scan indices that conincide with the beginning of a stimulus presentation
    
    for i = 1:length(stimulus_start_rows) % go through all stimuli/blocks
        
        % the G matrix contains # timebins many ones on a diagonal,
        % beginning "delay" many scans after the initial scan of the
        % current block
        row = stimulus_start_rows(i);
        start_row = row + delay; 
        end_row = row + delay + timebins - 1;
        start_col = (col-1)*timebins + 1;
        end_col = start_col + timebins - 1;
        
        % Put identity matrix in specified location to model one block
        subG(start_row:end_row,start_col:end_col) = subG(start_row:end_row,start_col:end_col) + eye(timebins, timebins);
        
    end
    
end

subG = subG > 0; % all entries greater than 0 are set to one
subG = subG(1:size(SIM.blocks,1),:); % cut rows exceeding the number of scans in the data

end