function [Z] = build_Z(sP)
%% Build Z matrix by arranging subject data matrices and normalizing columns subjectwise as in Metzack et al.

Z = zeros(sP.M*sP.nT, sP.nV*sP.nV);

for sub = 1:sP.M
    
    sub_data = load(simtb_makefilename(sP, 'DATA', sub)); % load data for subject sub
    
    sd = sub_data.D; % get "brain image"
    
    sd = zscore(sd,0,1); % normalize each voxel for that subject
    
    Z(((sub-1)*20 + 1):sub*20, :) = sd; % Z is a (#scans * #subjects) x #voxels matrix
    
end




end