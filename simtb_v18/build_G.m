function [G] = build_G(sP, delay, timebins)
%% Builds G Matrix for the data

% Initialize G matrix. It has #subjects*#scanspersubject 
% rows and #subject*#timebinsperblock*#blocks columns
G = zeros(sP.M*sP.nT,sP.M*timebins*sP.TC_block_n); 

row = 1;
col = 1;

for sub = 1:sP.M % go through all subjects
    
	% fill the subject specific submatrix calling build_sub_G
    G(row:(row+sP.nT-1), col:(col+(timebins*sP.TC_block_n)-1)) = build_sub_G(sP, sub, delay, timebins);
    
    row = row + sP.nT; % next subject begins #scanspersubject many rows below
    col = col + timebins*sP.TC_block_n; % next subject begins #timebins*#blocks many columns to the right
    
end

% %for debugging only:

%I = mat2gray(G);
%imshow(I);

end