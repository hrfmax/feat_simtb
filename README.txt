This folder contains our preliminary attempt to simulate more realistic fMRI data, using the specifically designed MATLAB toolbox SimTB (Allen et al. 2012).

Relevant Files and Directories:

-	Allen et al. 2012.pdf: Article from Neuroimage introducing the SimTB toolbox
-	CPCA-related/: Directory containing files from older simulation code for rotation etc.
-	MyParameterfile.m: Script that specifies all simulation parameters and stores them in a MATLAB variable sP
-	RunThisScript.m: Script that first runs the simulation using the parameters from MyParameterfile.m. Afterwards, CPCA and rotation is performed. 
-	SimTB Tutorial.pdf: Tutorial on SimTB
-	SimTB_Output/: Directory for SimTB to store simulated data
-	simtb_v18/: SimTB, modified for our purposes. Apart from adding three functions (see below), we modified simtb_makeTC_block.m not to randomize block order
-	simtb_v18/build_G.m: builds design matrix G for a given 
-	simtb_v18/build_sub_G.m: builds G matrix for a single subject in a given simulation
-	simtb_v18/build_Z.m: builds Z matrix from simulated data

Unfortunately, so far the approach using SimTB is not fully successful. Even for zero noise, CPCA (and rotation) does not separate the components we put into the simulation. Although CPCA seems to get the number of components inherent in the data right, it does neither separate the HRF shapes nor the brain areas. We have added some diagnostic plots to a short demo-script to illustrate this behavior. We have not found out the reason for this.