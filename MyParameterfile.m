%% OUTPUT PARAMETERS
%-------------------------------------------------------------------------------
% Directory to save simulation parameters and output
out_path = 'SimTB_Output';

prefix = 'block';  % Prefix for saving output

verbose_display = 0; % Show simulated data visually

%-------------------------------------------------------------------------------

%% RANDOMIZATION
%-------------------------------------------------------------------------------
seed = round(sum(100*clock));   % randomizes parameter values
%seed = 3571;                   % choose seed for repeatable simulation
simtb_rand_seed(seed);          % set the seed 
%-------------------------------------------------------------------------------

%% SIMULATION DIMENSIONS
%-------------------------------------------------------------------------------
M  = 10;   % number of subjects    
% nC is the number of components defined below, nC = length(SM_source_ID);
nV = 50; % number of voxels; dataset will have [nV x nV] voxels.           
nT = 20; % number of time points per subject, e.g., conditions * timebins
TR = 2;   % repetition time 
%-------------------------------------------------------------------------------

%% SPATIAL SOURCES
%-------------------------------------------------------------------------------
% Choose the sources. To launch a stand-alone GUI:
% >> simtb_pickSM 
%SM_source_ID = [ 1  2  3  4  5  6  7  8  9 10  ...
%                11 12 13 14 15 16 17 18 19 20  ...
%                21 22 23 24 25 26 27 28 29 30]; % all but (1, 10, 13)
SM_source_ID = [2 15];

nC = length(SM_source_ID);  % number of components            

% LABEL COMPONENTS
% Here, we label components or component groups that may be used later
% Auditory: strong positive activation for all task events
comp1  = find(SM_source_ID == 2);
comp2  = find(SM_source_ID == 15);
%comp3  = find(SM_source_ID == 3);
%comp4  = find(SM_source_ID == 4);

% compile list of all defined components of interest
complist = [comp1 comp2];
%-------------------------------------------------------------------------------

%% COMPONENT PRESENCE
%-------------------------------------------------------------------------------
% [M x nC] matrix for component presence: 1 if included, 0 otherwise
% For components not of interest there is a 90% chance of component inclusion.
SM_present = (rand(M,nC) < 0.9);
% Components of interest (complist) are included for all subjects.
SM_present(:,complist) = ones(M,length(complist));
%-------------------------------------------------------------------------------

%% SPATIAL VARIABILITY
%-------------------------------------------------------------------------------           
% Variability related to differences in spatial location and shape.
SM_translate_x = zeros(M,nC);  % Translation in x, mean 0, SD 0.1 voxels.
SM_translate_y = zeros(M,nC);  % Translation in y, mean 0, SD 0.1 voxels.
SM_theta       = zeros(M,nC);  % Rotation, mean 0, SD 1 degree.
%                Note that each 'activation blob' is rotated independently.
SM_spread = ones(M,nC); % Spread < 1 is contraction, spread > 1 is expansion.
%-------------------------------------------------------------------------------

%% TC GENERATION
%-------------------------------------------------------------------------------
% Choose the model for TC generation.  To see defined models:
% >> simtb_countTCmodels

TC_source_type = ones(1,nC);    % convolution with HRF for all components

TC_source_params = cell(M,nC);  % initialize the cell structure

% Set HRF PARAMETERS
% p    - 1x7 vector of parameters for the response function (two gamma functions)
%	p(1) - delay of response (relative to onset)	  (6)
%	p(2) - delay of undershoot (relative to onset)    (16)
%	p(3) - dispersion of response			          (1)
%	p(4) - dispersion of undershoot			          (1)
%	p(5) - ratio of response to undershoot            (6)
%	p(6) - onset (seconds)				              (0)
%	p(7) - length of kernel (seconds)	              (32)

% Component 1
p_comp1 = [2 16 1 1 3 0 20];
p_comp2 = [3 13 1 1 2 5 20];
%p_comp3 = [5 13 2 2 2 1 20];
%p_comp4 = [7 13 2 2 4 5 20];

TC_source_params(:,1) = mat2cell(p_comp1);
TC_source_params(:,2) = mat2cell(p_comp2);
%TC_source_params(:,3) = mat2cell(p_comp3);
%TC_source_params(:,4) = mat2cell(p_comp4);

%-------------------------------------------------------------------------------

%% EXPERIMENT DESIGN
%-------------------------------------------------------------------------------
% BLOCKS
TC_block_n = 2;          % Number of blocks [set = 0 for no block design]
TC_block_same_FLAG = 1;  % 1 = block structure same for all subjects
TC_block_length = 8;    % length of each block (in samples)
TC_block_ISI    = 2;     % length of OFF inter-stimulus-intervals (in samples)

TC_block_amp    = zeros(nC, TC_block_n); % initialize [nC x TC_block_n] matrix
TC_block_amp(1,1) = 1;   % Comp 1 is strongly modulated by condition 1
TC_block_amp(1,2) = 0; % Comp 1 is weakly modulated by condition 2
TC_block_amp(2,1) = 0;  % Comp 2 is negatively modulated by condition 1
TC_block_amp(2,2) = 1; % Comp 2 is strongly modulated by condition 2
%-------------------------------------------------------------------------------

%% UNIQUE EVENTS
%-------------------------------------------------------------------------------
TC_unique_FLAG = 0; % 1 = include unique events
%TC_unique_prob = 0.2*ones(1,nC); % [1 x nC] prob of unique event at each TR

%TC_unique_amp  = ones(M,nC);     % [M x nC] matrix of amplitude of unique events
% smaller unique activations for task-modulated components
%TC_unique_amp(:,comp1)              = 0.2;
%TC_unique_amp(:,comp2)              = 0.1;

%-------------------------------------------------------------------------------

%% DATASET BASELINE                                 
%-------------------------------------------------------------------------------
% [1 x M] vector of baseline signal intensity for each subject
D_baseline = 800*ones(1,M); % [1 x M] vector of baseline signal intensity
%-------------------------------------------------------------------------------

%% TISSUE TYPES
%-------------------------------------------------------------------------------
% FLAG to include different tissue types (distinct baselines in the data)
D_TT_FLAG = 0;                    % if 0, baseline intensity is constant 
%-------------------------------------------------------------------------------

%% PEAK-TO-PEAK PERCENT SIGNAL CHANGE 
%-------------------------------------------------------------------------------
D_pSC = ones(M,nC) %+ 0.25*randn(M,nC);   % [M x nC] matrix of percent signal changes 
%-------------------------------------------------------------------------------

%% NOISE
%-------------------------------------------------------------------------------
D_noise_FLAG = 0;               % FLAG to add rician noise to the data
% [1 x M] vector of contrast-to-noise ratio for each subject
% CNR is distributed as uniform between 0.65 and 2.0 across subjects.  
%%% JAN
% minCNR = 0.65;  maxCNR = 2;
% D_CNR = rand(1,M)*(maxCNR-minCNR) + minCNR; 
D_CNR = ones(1,M); 
%-------------------------------------------------------------------------------

%% MOTION 
%-------------------------------------------------------------------------------
D_motion_FLAG = 0;              % 1=motion, 0=no motion
%-------------------------------------------------------------------------------
% END of parameter definitions