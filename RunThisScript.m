% Run this Script to 
%   first:  simulate data according to Parameters specified in MyParameterfile.m
%   second: run CPCA on simulated data
%   third:  perform rotations

addpath(genpath('~/Documents/MATLAB/cpca_1.2.1.08i_Oct_2013')) % Include path to fMRI CPCA code here
addpath(genpath('CPCA-related'));
addpath(genpath('simtb_v18'));

%% Simulate Data using SimTB
sP = simtb_create_sP('MyParameterfile'); % load simulation parameters specified in MyParameterfile.m
[errorflag, Message] = simtb_checkparams(sP); % check parameters for consistency etc.
disp(Message)
simtb_main(sP) % simulate data
close all

%% Build G and Z Matrix using sP
G = build_G(sP, 0, 8); % build_G(sP, delay, timebins)
Z = build_Z(sP); % build Z matrix from simulation

%% CPCA

cpca_components = 2; % number of components to be extracted in SVD

C = inv(G'*G)*G'*Z; % CPCA regression step
GC = G*C;
[U D V] = svds(GC,cpca_components); % Extract cpca_components many components
P = pinv(G)*U; % calculate unrotated P matrix

%% Rotation
events = {[2 16 1 1 3 0 20], [3 13 1 1 2 5 20]}; % cell of suspected events, same specification scheme as in SimTB
target_shapes = get_target_shapes(sP.M,8,2,sP.TR,events); % get_target_shapes(#subjects,#timebins,#conditions,TR,events)
combos = nchoosek(1:size(target_shapes,2), cpca_components); % all combinations of target_shapes
[p_procr, T_procr, combo_procr, p_obl, T_obl, combo_obl] = my_procrustes(P, target_shapes, combos); % Procrustes Rotation

%% Plot results for one subject for diagnostic reasons
sub1 = load('SimTB_Output/block_subject_001_SIM.mat');
subplot(4,2,1)
plot(sub1.TC) % the ground truth timecourse activity for subject_001 and both components over time
title('component activity over time')
subplot(4,2,2)
plot(sub1.SM') % the ground truth voxel loadings for subject_001 and both components
title('''ground truth V matrix''')
subplot(4,2,3)
plot(P(1:20,:)) % the P matrix for the first subject
title('unrotated P matrix from CPCA')
subplot(4,2,4)
plot(V) % the V matrix for all subjects
title('unrotated V matrix from CPCA')
subplot(4,2,5)
plot(p_procr(1:20,:)) % procrustes rotated P for the first subject
title('procrustes rotated P from CPCA')
subplot(4,2,6)
plot(V*T_procr) % procrustes rotated V 
title('procrustes rotated V matrix from CPCA')
subplot(4,2,7)
plot(p_obl(1:20,:)) % oblique procrustes rotated P for the first subject
title('obl rotated P from CPCA')
subplot(4,2,8)
plot(V*T_obl) % oblique procrustes rotated V
title('obl rotated V matrix from CPCA')