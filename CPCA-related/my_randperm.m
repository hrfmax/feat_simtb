function [rp]=my_randperm(v, k)
% MY_RANDPERM implements costum verion of the MATLAB randperm function
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

[n,m] = size(v);

n = max(n,m);

mask = zeros(1,n);
all_idx = 1:n;
i=0;
while i<k && ~isempty(all_idx)
    i=i+1;
    which_idx_in_poss_idx = randi(length(all_idx), 1, 1);
    mask(which_idx_in_poss_idx) = 1;
    all_idx(which_idx_in_poss_idx) = [];
    
end
   
rp = v(logical(mask));
end
