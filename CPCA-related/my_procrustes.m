function [best_procr_p best_procr_t best_combo_procr best_obl_p best_obl_t best_combo_obl] = my_procrustes(p_unrot, shapes, combos)
% MY_PROCRUSTES performs orthogonal and oblique procrustes rotation for HRFMAX
% This function finds the best transformation to rotate the input predictor
% weights to be as similar as possible to a subset of shapes from the
% matrix containing the target shapes. It takes into account all possible
% combinations of shapes in the current number of components and returns
% the one combination that yields the best fit. 
% Formally, the orthogonal procrustes rotation was defined by Schönemann,
% 1966. The oblique one was established by Hurley and Cartell 1962. 
% INPUT 
%   p_unrot     the unrotated predictor weights from CPCA
%   shapes      matrix of target shapes (columns)
%   combos      matrix of indeces with all possible combinations to take
%               components many shapes from the shapes matrix
% OUTPUT
%   best_procr_p        best orthogobally rotated predictor weight matrix
%   best_procr_t        rotation matrix of the best orthognal rotation
%   best_combo_procr    combination that yielded the best orthogonal fit
%   best_obl_p          best obliquely rotated predictor weight matrix
%   best_obl_t          rotation matrix of the best oblique rotation
%   best_combo_obl      combination that yielded the best oblique fit
%
% @author Konstantin Helmsauer, Jan Boelts
% {konstantinhelmsauer}{j.f.boelts}@gmail.com

% get number of components
num_components = size(p_unrot,2);

% prelocate with default values
best_procr_p = p_unrot;
best_procr_t = eye(num_components);
best_cor_orth = 0;
best_ss_obl = Inf;
timing_procr = 0;
timing_obl = 0;

% for each possible combination
for i = 1:size(combos,1)
    
    % get current combination of shapes
    p_model = shapes(:,combos(i,:));
    tic;
    %% Orthogonal Procrustes Rotation
    [L, dummyvariable, M] = svd(p_model' * p_unrot);
    t_procr = M * L';
    p_procr = p_unrot * t_procr;
    
    %% Goodness of Fit -- Orthogonal Rotation
    
    % correlate columns
    y = abs(corrcoef([p_procr p_model]));
    % get relevant correlation coeffs
    y = y((num_components+1):end, 1:num_components);
    % take the maximum because order of components is random
    p_procr_volume_cor = prod(max(y, [], 1));
    
    % check whether current is better than sofar best
    if p_procr_volume_cor > best_cor_orth
        % set new best version
        best_procr_p = p_procr;
        best_procr_t = t_procr;
        best_cor_orth = p_procr_volume_cor;
        best_combo_procr = combos(i,:);
    end
    % measure the elapsed time
    timing_procr = timing_procr + toc;
    
    %% Oblique Procrustes Rotation
    tic;
    % check the neccessary condition of full rank 
    if (rank(p_model) == num_components)
        % perform oblique rotation
        t_obl = p_unrot \ p_model;
        t_obl = t_obl * diag(sqrt(diag((t_obl'*t_obl)\eye(size(p_model,2)))));
        p_obl = p_unrot * t_obl;
        
        % Ensure that rotated p contains only real numbers
        if isreal(p_obl)
            
            %% Goodness of Fit -- Oblique Rotation
            % here take sum of squares as a measure of fit
            sum_squares_obl = sum(sum((p_model-p_obl).^2));
            
            % check if better than before
            if sum_squares_obl < best_ss_obl
                best_obl_p = p_obl;
                best_obl_t = t_obl;
                best_ss_obl = sum_squares_obl;
                best_combo_obl = combos(i,:);
            end
            
        end
        
    end
    % measure elapsed time
    timing_obl = timing_obl + toc;
end